﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RigController : MonoBehaviour {
    float speed = 10.0f;
    Vector2 moveVector;
    Rigidbody2D rig;
    bool canJump;
    // Use this for initialization
    void Start () {
        rig = gameObject.GetComponent<Rigidbody2D>();
        canJump = true;
	}
	
	// Update is called once per frame
	void Update () {
       if (transform.position.y <= -1.5f)
            canJump = true;
        moveVector = Vector2.zero;

        moveVector.x = Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime;

        if(moveVector.x!=0)
        {
            if (moveVector.x < 0)
                gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
            else
                gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }

        
        rig.MovePosition(rig.position + moveVector);

        if (Input.GetKey(KeyCode.Space)&& canJump && transform.position.y < 2)
            rig.AddForce(Vector2.up * 2000);
        if (transform.position.y >= 2)
            canJump = false;

    }

}
